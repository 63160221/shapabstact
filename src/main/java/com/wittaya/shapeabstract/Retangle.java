/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapeabstract;

/**
 *
 * @author AdMiN
 */
public class Retangle extends Shape {
    private double w;
    private double h;
    public Retangle(double w, double h) {
        super("Retangle");
        this.w = w;
        this.h = h;
    }public double getW() {
        return w;
    }public double getH() {
        return h;
    }public void setW(double w) {
        this.w=w;
    }public void setH(double h) {
        this.h=h;
    }@Override
    public double calArea() {
        return w*h;
    }@Override
    public String toString() {
        return "Rectangle{" + "w=" + w + ", h=" + h + '}';
    }

}











