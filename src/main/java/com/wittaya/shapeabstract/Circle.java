/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapeabstract;

/**
 *
 * @author AdMiN
 */
public class Circle extends Shape {
    private double r;
    private static final double pi = 22.0 / 7.0;
public Circle(double r) {
        super("Circle");
        this.r = r;
    }
public double getR() {
        return r;
    }
public void setR(double r) {
        this.r = r;
    }
@Override
    public double calArea() {
        return pi*r*r;
    }
@Override
    public String toString() {
        return "Circle{"  +  "r="  +  r  + '}';
    }
}
